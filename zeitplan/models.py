from datetime import datetime

from django.contrib.auth.models import User
from django.db import models

from zeitplan.helper_functions import get_german_day_name


class DayManager(models.Manager):
    def create_day(self, day_date: datetime.date, user: User) -> "Day":
        day = self.create(day_date=day_date, user=user)
        day.day_name_german = get_german_day_name(day.day_date)
        day.save()

        return day


class Day(models.Model):
    day_date = models.DateField(null=True, default=None)
    day_name_german = models.CharField(max_length=100, blank=True)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    objects = DayManager()

    @property
    def different_day_date(self) -> str:
        diff_day_date = self.day_date.strftime("%d" "." "%m" "." "%Y")

        return diff_day_date

    def __str__(self) -> str:
        return f"Datum: {self.day_date}"


class EntryCategoryManager(models.Manager):
    def create_entry_category(self, category_text: str) -> "EntryCategory":
        entry_category = self.create(category_text=category_text)

        return entry_category


class EntryCategory(models.Model):
    category_text = models.CharField(max_length=100)

    objects = EntryCategoryManager()

    def __str__(self) -> str:
        return f"Kategorie: {self.category_text}"


class TimeEntryQuerySet(models.QuerySet):
    def time_entry_for_a_day(self, day_id: int) -> "TimeEntryQuerySet":
        time_entry_for_a_day = self.filter(day_id=day_id).order_by("start_of_entry")

        return time_entry_for_a_day


class TimeEntryManager(models.Manager):
    def create_time_entry(self, day: Day, entry_text: str, user: User) -> "TimeEntry":
        time_entry = self.create(day=day, entry_text=entry_text, user=user)

        return time_entry


class TimeEntry(models.Model):
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    entry_category = models.ForeignKey(
        EntryCategory, models.SET_NULL, blank=True, null=True
    )
    entry_text = models.CharField(max_length=100)
    entry_passed = models.BooleanField(null=True, default=None)
    votes = models.IntegerField(default=0)
    start_of_entry = models.DateTimeField(null=True, default=None)
    end_of_entry = models.DateTimeField(null=True, default=None)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    objects = TimeEntryManager.from_queryset(TimeEntryQuerySet)()

    @property
    def entry_status(self) -> str:
        if self.entry_passed is None:
            entry_status = "Nicht erledigt"
        else:
            entry_status = "Erledigt"

        return entry_status

    @property
    def proper_time_format(self) -> tuple[str, str]:
        if self.start_of_entry and self.end_of_entry is not None:
            start = self.start_of_entry.strftime("%H" ":" "%M")
            end = self.end_of_entry.strftime("%H" ":" "%M")

        else:
            start, end = "", ""

        return start, end

    def __str__(self) -> str:
        return f"Tätigkeit: {self.entry_text}"
