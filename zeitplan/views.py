from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from zeitplan.helper_functions import convert_str_to_datetime_date
from zeitplan.models import Day, EntryCategory, TimeEntry


def index(request) -> render:
    return render(request, "zeitplan/index.html")


def get_time_entry_list(day_id: int) -> dict:
    day = Day.objects.get(pk=day_id)

    time_entry_list = TimeEntry.objects.time_entry_for_a_day(day_id=day_id)

    context = {
        "day": day,
        "time_entry_list": time_entry_list,
    }

    return context


@login_required
def overview(request) -> render:
    all_days = Day.objects.all().order_by("day_date")
    context = {"all_days_list": all_days}

    return render(request, "zeitplan/overview.html", context)


@login_required
def day_overview(request, day_id: int) -> render:
    context = get_time_entry_list(day_id)

    return render(request, "zeitplan/day_overview.html", context)


@login_required
def day_edit(request, day_id: int) -> render:
    context = get_time_entry_list(day_id)

    return render(request, "zeitplan/day_editing.html", context)


@login_required
def day_editing(request, day_id: int) -> dict | HttpResponseRedirect:
    try:
        if "entry_vote" in request.POST:
            entries = Day.objects.get(pk=day_id).timeentry_set.get(
                pk=request.POST["entry_vote"]
            )
        elif "entry_passed" in request.POST:
            entries = Day.objects.get(pk=day_id).timeentry_set.get(
                pk=request.POST["entry_passed"]
            )
        elif "entry_delete" in request.POST:
            entries = Day.objects.get(pk=day_id).timeentry_set.get(
                pk=request.POST["entry_delete"]
            )

    except (KeyError, Day.DoesNotExist):
        context = {"day": Day.objects.get(pk=day_id), "error message": "Why?"}

        return render(request, "zeitplan/day_editing.html", context)

    else:
        if "entry_vote" in request.POST:
            entries.votes += 1
            entries.save()
        elif "entry_passed" in request.POST:
            if entries.entry_passed is None:
                entries.entry_passed = True
            else:
                entries.entry_passed = None
            entries.save()
        elif "entry_delete" in request.POST:
            entries.delete()

        return HttpResponseRedirect(
            reverse("zeitplan:day_edit", args=(Day.objects.get(pk=day_id).id,))
        )


@login_required
def category_add(request, entry_id: int) -> HttpResponseRedirect:
    entry = TimeEntry.objects.get(pk=entry_id)

    new_category = EntryCategory.objects.create_entry_category(
        category_text=request.POST["category_add"]
    )

    entry.entry_category = new_category
    entry.save()

    return HttpResponseRedirect(reverse("zeitplan:day_edit", args=(entry.day.id,)))


@login_required
def add_time_frame(request, entry_id: int) -> HttpResponseRedirect:
    entry = TimeEntry.objects.get(pk=entry_id)

    entry.start_of_entry = "%sT%s" % (
        entry.day.day_date,
        request.POST["add_time_frame_start"],
    )
    entry.end_of_entry = "%sT%s" % (
        entry.day.day_date,
        request.POST["add_time_frame_end"],
    )

    entry.save()

    return HttpResponseRedirect(reverse("zeitplan:day_edit", args=(entry.day.id,)))


@login_required
def day_new(request) -> render:
    return render(request, "zeitplan/day_new.html")


@login_required
def day_add_new(request):
    day_date_in_datetime_date = convert_str_to_datetime_date(request.POST["new_day"])

    new_day = Day.objects.create_day(
        day_date=day_date_in_datetime_date, user=request.user
    )

    return HttpResponseRedirect(reverse("zeitplan:overview"))


@login_required
def day_delete(request, day_id: int) -> HttpResponseRedirect:
    day_deleting = Day.objects.get(pk=day_id)
    day_deleting.delete()

    return HttpResponseRedirect(reverse("zeitplan:overview"))


@login_required
def time_entry_add(request, day_id: int) -> HttpResponseRedirect:
    new_entry = TimeEntry.objects.create_time_entry(
        day=Day.objects.get(pk=day_id),
        entry_text=request.POST["entry_add"],
        user=request.user,
    )

    return HttpResponseRedirect(reverse("zeitplan:day_edit", args=(new_entry.day.id,)))
