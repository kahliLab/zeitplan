from datetime import datetime

GERMAN_TRANSLATION = {
    "Monday": "Montag",
    "Tuesday": "Dienstag",
    "Wednesday": "Mittwoch",
    "Thursday": "Donnerstag",
    "Friday": "Freitag",
    "Saturday": "Samstag",
    "Sunday": "Sonntag",
}

FORMAT = "%Y-%m-%d"


def get_german_day_name(day_date: datetime.date) -> str:
    german_day_name = GERMAN_TRANSLATION[day_date.strftime("%A")]

    return german_day_name


def convert_str_to_datetime_date(date_as_string: str) -> datetime.date:
    date = datetime.strptime(date_as_string, FORMAT).date()

    return date
